package com.example.pokemon_app.teamList

interface Listener {
    fun setEmptyListTop(visibility: Boolean)

    fun setEmptyListBottom(visibility: Boolean)
}