package com.example.pokemon_app.roomDB.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.pokemon_app.roomDB.entity.MoveEntity
import com.example.pokemon_app.roomDB.entity.PokemonEntity

@Dao
interface IMoveDAO {
    @Query("SELECT * FROM move_table WHERE name = :move")
    fun getMoveEntity(move: String): MoveEntity

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertMoveEntity(move: MoveEntity)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertMoveEntity(moveList: List<MoveEntity>)
}