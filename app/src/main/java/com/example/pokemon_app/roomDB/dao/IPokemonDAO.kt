package com.example.pokemon_app.roomDB.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.pokemon_app.roomDB.entity.PokemonEntity

@Dao
interface IPokemonDAO {
    @Query("SELECT * FROM pokemon_table WHERE species = :species")
    fun getPokemonEntity(species: String): PokemonEntity

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertPokemonEntity(pokemon: PokemonEntity)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertPokemonEntity(pokemonList: List<PokemonEntity>)


}