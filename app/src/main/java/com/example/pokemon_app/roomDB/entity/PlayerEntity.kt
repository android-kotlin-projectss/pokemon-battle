package com.example.pokemon_app.roomDB.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.example.pokemon_app.roomDB.Converters

@Entity(tableName = "player_table")
@TypeConverters(Converters::class)
data class PlayerEntity(
    @PrimaryKey val name: String,
    val pokemonTeam:  List<PokemonEntity>,
    val pokemonCollection:  List<PokemonEntity>
)